package com.crea.servlets;

import com.crea.dev2.a2.jee.ecole.model.beans.*;
import com.crea.dev2.a2.jee.ecole.model.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

@WebServlet(name = "ControleurPrincipal")
public class ControleurPrincipal extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Variable de récupération de l'action demandée
        String idform = request.getParameter("idaction"); //a partir de chaque formulaire HTML/XHTML/JSTL/JSP
        System.out.println("POST");
        System.out.println(idform);

        switch (idform) {
            case "getEleveByNum":
                // Récupération de l'élève via son numéro

                // Récupération du parametre contenant le num de l'élève
                String numelev = request.getParameter("numelev");

                try {
                    // Récupération depuis la db de l'élève correspondant
                    Eleve eRecup = EleveDAO.getEleveByNum(numelev);

                    // Affichage dans la console de l'élève récupéré
                    eRecup.affiche();

                    // Ajout de l'élève récupéré aux attributs renvoyés à la page
                    request.setAttribute("eleveRecup", eRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "updateEleveByNum":
                // Modification de l'élève via son numéro

                // Récupération du parametre contenant le num de l'élève
                String numUpdate = request.getParameter("numEleve");

                // Récupération du parametre contenant le nouveau no de l'élève
                int noUpdate = Integer.parseInt(request.getParameter("noEleve"));

                // Récupération du paramètre contenant l'adresse de l'élève
                String adresseUpdate = request.getParameter("adresseEleve");

                try {
                    // Récupération depuis la db de l'élève correspondant
                    int codeUpdateAdresse = EleveDAO.updAdresseEleveByNum(numUpdate, adresseUpdate);

                    int codeUpdateChambre = EleveDAO.updNoChambreEleveByNum(numUpdate, noUpdate);

                    String updateEleveResult = "Une erreur s'est produite lors de la modification de l'élève";
                    int updateCode = codeUpdateAdresse + codeUpdateChambre;
                    if (updateCode == 2) {
                        updateEleveResult = "Eleve successfully updated";
                    }

                    System.out.println(updateEleveResult);
                    System.out.println("Code de l'update : " + updateCode);


                    // Ajout du code et du résultat de la requete aux attributs
                    request.setAttribute("updateCode", updateCode);
                    request.setAttribute("updateEleveResult", updateEleveResult);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "getEleveByNom":
                // Récupération d'une liste d'élèves via leur noms (élèves avec le meme nom)

                // Récupération du parametre contenant le num de l'élève
                String nomEleve = request.getParameter("nomEleve");

                try {
                    // Récupération depuis la db de l'élève correspondant
                    ArrayList<Eleve> eListRecup = EleveDAO.getEleveByNom(nomEleve);


                    // Ajout des l'élève récupéré aux attributs renvoyés à la page
                    request.setAttribute("eleveListRecupByNom", eListRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "addEleve":
                // Ajout d'un nouvel élève

                // Récupération du parametre contenant le num de l'élève
                String num = request.getParameter("numEleve");
                // Récupération du parametre contenant le nom de l'élève
                String nom = request.getParameter("nomEleve");
                // Récupération du paramètre contenant l'age de l'élève
                int age = Integer.parseInt(request.getParameter("ageEleve"));
                // Récupération du parametre contenant l'adresse de l'élève
                String adresse = request.getParameter("adresseEleve");

                // Ajout d'un nouvel élève dans la database et récupération du code de l'opération
                int code = EleveDAO.addEleve(num, nom, age, adresse);

                String addEleveResult = "Une erreur s'est produite";

                // Affichage du status de l'opération
                switch (code) {
                    case 1:
                        addEleveResult = "Élève ajouté avec succès";
                        break;
                    case 0:
                        addEleveResult = "Aucun élève ajouté";
                    case -1:
                        addEleveResult = "Une erreur s'est produite";
                }

                // Affichage console du résultat de l'ajout de l'élève
                System.out.println(addEleveResult);

                // Ajout du résultat de l'ajout de l'élève + code aux attributs renvoyés à la page
                request.setAttribute("addEleveResult", addEleveResult);
                request.setAttribute("addCode", code);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + code);
                break;

            case "deleteEleveByNum":
                // Suppression d'un élève via son numéro

                // Récupération du parametre contenant le num de l'élève
                String numToDelete = request.getParameter("numEleve");

                // Suppression de l'élève dans la database et récupération du code de l'opération
                int codeDelete = EleveDAO.deleteEleveByNum(numToDelete);

                String deleteEleveResult = "";

                // Affichage du status de l'opération
                switch (codeDelete) {
                    case 1:
                        deleteEleveResult = "Élève supprimé avec succès";
                        break;
                    case 0:
                        deleteEleveResult = "Aucun élève supprimé";
                    case -1:
                        deleteEleveResult = "Une erreur s'est produite";
                }

                // Ajout du résultat de la suppression de l'élève + code aux attributs renvoyés à la page
                request.setAttribute("deleteEleveResult", deleteEleveResult);
                request.setAttribute("deleteCode", codeDelete);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + codeDelete);

                break;

            case "getListEleveByDateN":
                // Récupération des élève ayant la meme date de naissance

                // Récupération du parametre contenant le num de l'élève
                int anneeNaissance = Integer.parseInt(request.getParameter("anneeNaissance"));

                try {
                    // Récupération depuis la db de l'élève correspondant
                    ArrayList<Eleve> eListRecupDate = EleveDAO.getListEleveByDateN(anneeNaissance);

                    // Ajout des l'élève récupéré aux attributs renvoyés à la page
                    request.setAttribute("eleveListRecupByAge", eListRecupDate);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "getAllEleve":
                try {
                    // Récupération depuis la db de la list complète des élèves
                    ArrayList<Eleve> eleveListAll = EleveDAO.getAllEleve();

                    // Ajout de la liste des élève aux attributs renvoyés à la page
                    request.setAttribute("eleveListAll", eleveListAll);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "addInscrit":
                // Récupération du parametre contenant le num de l'élève
                String numEleveInscrit = request.getParameter("numEleve");
                // Récupération du parametre contenant le code de l'inscrit
                String codeInscrit = request.getParameter("codeInscrit");
                // Récupération du paramètre contenant la note de l'inscrit
                float noteInscrit = Float.parseFloat((request.getParameter("noteInscrit")));


                // Ajout d'un nouvel inscrit dans la database et récupération du code de l'opération
                int addInscritCode = InscritDAO.addInscrit(codeInscrit, numEleveInscrit, noteInscrit);

                String addInscritResult = "Une erreur s'est produite";

                // Affichage du status de l'opération
                switch (addInscritCode) {
                    case 1:
                        addInscritResult = "Inscrit ajouté avec succès";
                        break;
                    case 0:
                        addInscritResult = "Aucun Inscrit ajouté";
                    case -1:
                        addInscritResult = "Une erreur s'est produite";
                }

                // Affichage console du résultat de l'ajout de l'inscrit
                System.out.println(addInscritResult);

                // Ajout du résultat de l'ajout de l'inscrit + code aux attributs renvoyés à la page
                request.setAttribute("addInscritResult", addInscritResult);
                request.setAttribute("addInscritCode", addInscritCode);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + addInscritCode);
                break;

            case "deleteInscritByCode":
                // Suppression d'un inscrit via son code

                // Récupération du parametre contenant le code de l'inscrit
                String inscritCodeToDelete = request.getParameter("codeInscrit");

                // Suppression de l'inscrit dans la database et récupération du code de l'opération
                int deleteInscritCode = InscritDAO.deleteInscritByCode(inscritCodeToDelete);

                String deleteInscritResult = "";

                // Affichage du status de l'opération
                switch (deleteInscritCode) {
                    case 1:
                        deleteInscritResult = "Inscrit supprimé avec succès";
                        break;
                    case 0:
                        deleteInscritResult = "Aucun inscrit supprimé";
                    case -1:
                        deleteInscritResult = "Une erreur s'est produite";
                }

                // Ajout du résultat de la suppression de l'inscrit + code aux attributs renvoyés à la page
                request.setAttribute("deleteInscritResult", deleteInscritResult);
                request.setAttribute("deleteInscritCode", deleteInscritCode);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + deleteInscritCode);

                break;
            case "getInscritByCode":
                // Récupération de l'inscrit via son code

                // Récupération du parametre contenant le code de l'inscrit
                String codeGetInscrit = request.getParameter("codeInscrit");

                try {
                    // Récupération depuis la db de l'inscrit correspondant
                    Inscrit iRecup = InscritDAO.getInscritByCode(codeGetInscrit);

                    // Ajout de l'inscrit récupéré aux attributs renvoyés à la page
                    request.setAttribute("inscritRecup", iRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "getAllInscrit":
                try {
                    // Récupération dans la db de la liste complète des inscrits
                    Vector<Inscrit> inscritListAll = InscritDAO.getAllInscrit();

                    // Ajout de la liste des inscrits aux attributs renvoyés à la page
                    request.setAttribute("inscritListAll", inscritListAll);

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                break;
            case "updateInscritByCodeOrNum":
                // Modification de l'inscrit via son code ou son num

                // Récupération du parametre contenant le num de l'inscrit
                String numInscritUpdate = request.getParameter("numInscrit");

                // Récupération du parametre contenant la nouvelle note de l'inscri
                float noteUpdate = Float.parseFloat(request.getParameter("noteInscrit"));

                // Récupération du paramètre contenant le code de l'inscrit
                String codeInscritUpdate = request.getParameter("codeInscrit");

                try {

                    int codeUpdateNum = InscritDAO.updateNumByCode(codeInscritUpdate, numInscritUpdate);

                    int codeUpdateNote = InscritDAO.updateNotebyNum(numInscritUpdate, noteUpdate);

                    String updateInscritResult = "Une erreur s'est produite lors de la modification de l'inscrit";
                    int updateCodeInscrit = codeUpdateNum + codeUpdateNote;

                    if (updateCodeInscrit == 2) {
                        updateInscritResult = "Inscrit successfully updated";
                    }

                    System.out.println(updateInscritResult);
                    System.out.println("Code de l'update : " + updateCodeInscrit);


                    // Ajout du code et du résultat de la requete aux attributs
                    request.setAttribute("updateInscritCode", updateCodeInscrit);
                    request.setAttribute("updateInscritResult", updateInscritResult);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "getChambreByNo":
                // Récupération de la chambre via son no

                // Récupération du parametre contenant le no de chambre
                int noGetChambre = Integer.parseInt(request.getParameter("noChambre"));

                try {
                    // Récupération depuis la db de la chambre correspondante
                    Chambre cRecup = ChambreDAO.getChambreByNo(noGetChambre);

                    // Ajout de la chambre récupérée aux attributs renvoyés à la page
                    request.setAttribute("chambreRecup", cRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "deleteChambreByNo":
                // Suppression d'une chambre via son no

                // Récupération du parametre contenant le no de chambre
                int noToDelete = Integer.parseInt(request.getParameter("noChambre"));
                try {
                    Chambre chambre = ChambreDAO.getChambreByNo(noToDelete);
                    Eleve eleve = EleveDAO.getEleveByNum(chambre.getNum());

                    // Enlever le num de la chambre afin de pouvoir delete
                    ChambreDAO.updateNumByNo(noToDelete);

                    // Remove no chambre de l'élève
                    EleveDAO.updNoChambreEleveByNum(eleve.getNum());

                } catch (SQLException e) {
                    e.printStackTrace();
                }


                // Suppression de la chambre dans la database et récupération du code de l'opération
                int codeDeleteChambre = ChambreDAO.deleteChambreByNo(noToDelete);

                String deleteChambreResult = "";

                // Affichage du status de l'opération
                switch (codeDeleteChambre) {
                    case 1:
                        deleteChambreResult = "Chambre supprimée avec succès";
                        break;
                    case 0:
                        deleteChambreResult = "Aucune Chambre supprimée";
                    case -1:
                        deleteChambreResult = "Une erreur s'est produite";
                }

                // Ajout du résultat de la suppression de la chambre + code aux attributs renvoyés à la page
                request.setAttribute("deleteChambreResult", deleteChambreResult);
                request.setAttribute("deleteChambreCode", codeDeleteChambre);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + codeDeleteChambre);

                break;
            case "addChambre":
                // Récupération du parametre contenant le no chambre
                int noChambreAdd = Integer.parseInt(request.getParameter("noChambre"));
                // Récupération du parametre contenant le num
                String numChambreAdd = request.getParameter("numChambre");
                // Récupération du paramètre contenant la note de l'inscrit
                float prixChambreAdd = Float.parseFloat((request.getParameter("prixChambre")));


                // Ajout d'une nouvelle chambre dans la database et récupération du code de l'opération
                int addChambreCode = ChambreDAO.addChambre(noChambreAdd, numChambreAdd, prixChambreAdd);

                String addChambreResult = "Une erreur s'est produite";

                // Affichage du status de l'opération
                switch (addChambreCode) {
                    case 1:
                        addChambreResult = "Chambre ajoutée avec succès";
                        break;
                    case 0:
                        addChambreResult = "Aucune chambre ajouté";
                    case -1:
                        addChambreResult = "Une erreur s'est produite";
                }

                // Affichage console du résultat de l'ajout de la chambre
                System.out.println(addChambreResult);

                // Ajout du résultat de l'ajout de la chambre + code aux attributs renvoyés à la page
                request.setAttribute("addChambreResult", addChambreResult);
                request.setAttribute("addChambreCode", addChambreCode);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + addChambreCode);
                break;

            case "getAllChambre":
                try {
                    // Récupération dans la db de la liste complète des chambres
                    Vector<Chambre> chambreListAll = ChambreDAO.getAllChambre();

                    // Ajout de la liste des chambres aux attributs renvoyés à la page
                    request.setAttribute("chambreListAll", chambreListAll);

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                break;
            case "updateChambre":
                // Modification de la chambre via son code ou son num

                // Récupération du parametre contenant le num de l'inscrit
                String numChambreUpdate = request.getParameter("numChambre");

                // Récupération du parametre contenant le nouveau prix de la chambre
                float prixUpdate = Float.parseFloat(request.getParameter("prixChambre"));

                // Récupération du parametre contenant le code de l'inscrit
                int noUpdateChambre = Integer.parseInt(request.getParameter("noChambre"));


                try {

                    int codeUpdateNumChambre = ChambreDAO.updateNumByNo(numChambreUpdate, noUpdateChambre);

                    int codeUpdatePrix = ChambreDAO.updatePrixByNum(numChambreUpdate, prixUpdate);

                    String updateChambreResult = "Une erreur s'est produite lors de la modification de la chambre";
                    int updateCodeChambre = codeUpdateNumChambre + codeUpdatePrix;

                    if (updateCodeChambre == 2) {
                        updateChambreResult = "Chambre successfully updated";
                    }

                    System.out.println(updateCodeChambre);
                    System.out.println("Code de l'update : " + updateCodeChambre);


                    // Ajout du code et du résultat de la requete aux attributs
                    request.setAttribute("updateChambreCode", updateCodeChambre);
                    request.setAttribute("updateChambreResult", updateChambreResult);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "getUVByCode":
                // Récupération de l'uv via son code

                // Récupération du parametre contenant le code de l'uv
                String codeUVGet = request.getParameter("codeUV");

                try {
                    // Récupération depuis la db de l'uv correspondant
                    UV uvRecup = UVDAO.getUVByCode(codeUVGet);

                    // Ajout de l'uv récupéré aux attributs renvoyés à la page
                    request.setAttribute("uvRecup", uvRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "deleteUVByCode":
                // Delete un uv via son code
                String codeUVToDelete = request.getParameter("codeUV");

                // Suppression de l'uv dans la database et récupération du code de l'opération
                int codeUVDelete = UVDAO.deleteUVByCode(codeUVToDelete);

                String deleteUVResult = "";

                // Affichage du status de l'opération
                switch (codeUVDelete) {
                    case 1:
                        deleteUVResult = "UV supprimé avec succès";
                        break;
                    case 0:
                        deleteUVResult = "Aucun UV supprimé";
                    case -1:
                        deleteUVResult = "Une erreur s'est produite";
                }

                // Ajout du résultat de la suppression de l'uv + code aux attributs renvoyés à la page
                request.setAttribute("deleteUVResult", deleteUVResult);
                request.setAttribute("deleteUVCode", codeUVDelete);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + codeUVDelete);
                break;

            case "addUV":
                // Récupération du parametre contenant le nbh uv
                int nbhUVAdd = Integer.parseInt(request.getParameter("nbhUV"));
                // Récupération du parametre contenant le code uv
                String codeUVAdd = request.getParameter("codeUV");
                // Récupération du paramètre contenant la coord de l'uv
                String coordUVAdd = request.getParameter("coordUV");


                // Ajout d'une nouvelle chambre dans la database et récupération du code de l'opération
                int addUVCode = UVDAO.addUV(codeUVAdd, nbhUVAdd, coordUVAdd);

                String addUVResult = "Une erreur s'est produite";

                // Affichage du status de l'opération
                switch (addUVCode) {
                    case 1:
                        addUVResult = "UV ajouté avec succès";
                        break;
                    case 0:
                        addUVResult = "Aucun uv ajouté";
                    case -1:
                        addUVResult = "Une erreur s'est produite";
                }

                // Affichage console du résultat de l'ajout de l'uv
                System.out.println(addUVResult);

                // Ajout du résultat de l'ajout de l'uv + code aux attributs renvoyés à la page
                request.setAttribute("addUVResult", addUVResult);
                request.setAttribute("addUVCode", addUVCode);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + addUVCode);
                break;
            case "getAllUV":
                try {
                    // Récupération dans la db de la liste complète des uv
                    Vector<UV> uvListAll = UVDAO.getAllUV();

                    // Ajout de la liste des uv aux attributs renvoyés à la page
                    request.setAttribute("uvListAll", uvListAll);

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                break;
            case "updateUV":
                // Modification de l'uv via son code

                // Récupération du parametre contenant le code de l'uv
                String codeUVUpdate = request.getParameter("codeUV");

                // Récupération du parametre contenant le nouveau nbh de l'uv
                int nbhUVUpdate = Integer.parseInt(request.getParameter("nbhUV"));

                // Récupération du parametre contenant la coord de l'uv
                String coordUVUpdate = request.getParameter("coordUV");


                try {
                    int codeUpdateCoord = UVDAO.updateCoordByCode(codeUVUpdate, coordUVUpdate);

                    int codeUpdateNbh = UVDAO.updateNbhByCode(codeUVUpdate, nbhUVUpdate);

                    String updateUVResult = "Une erreur s'est produite lors de la modification de l'uv";
                    int updateCodeUV = codeUpdateCoord + codeUpdateNbh;

                    if (updateCodeUV == 2) {
                        updateUVResult = "UV successfully updated";
                    }

                    System.out.println(updateCodeUV);
                    System.out.println("Code de l'update : " + updateCodeUV);


                    // Ajout du code et du résultat de la requete aux attributs
                    request.setAttribute("updateUVCode", updateCodeUV);
                    request.setAttribute("updateUVResult", updateUVResult);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "addLivre":
                // Ajout d'un nouveau Livre

                // Récupération du parametre contenant le num de l'élève qui loue le livre
                String numLivre = request.getParameter("numLivre");
                // Récupération du parametre contenant le titre du Livre
                String titreLivre = request.getParameter("titreLivre");

                Date datePret = null;
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    datePret = (Date) formatter.parse(request.getParameter("datePret"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                // Récupération du parametre contenant la cote du Livre
                String coteLivre = request.getParameter("coteLivre");

                // Ajout d'un nouveau livre dans la database et récupération du code de l'opération
                int codeAddLivre = -1;
                try {
                    codeAddLivre = LivreDAO.addLivre(coteLivre, numLivre, titreLivre, datePret);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String addLivreResult = "Une erreur s'est produite";

                // Affichage du status de l'opération
                switch (codeAddLivre) {
                    case 1:
                        addLivreResult = "Livre ajouté avec succès";
                        break;
                    case 0:
                        addLivreResult = "Aucun livre ajouté";
                    case -1:
                        addLivreResult = "Une erreur s'est produite";
                }

                // Affichage console du résultat de l'ajout de l'élève
                System.out.println(addLivreResult);

                // Ajout du résultat de l'ajout de l'élève + code aux attributs renvoyés à la page
                request.setAttribute("addLivreResult", addLivreResult);
                request.setAttribute("addLivreCode", codeAddLivre);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + codeAddLivre);
                break;

            case "getLivreByCote":
                // Récupération du livre via sa cote

                // Récupération du parametre contenant la cote du livre
                String coteGetLivre = request.getParameter("coteLivre");

                try {
                    // Récupération depuis la db du livre correspondant
                    Livre livreRecup = LivreDAO.getLivreByCote(coteGetLivre);

                    // Ajout du livre récupéré aux attributs renvoyés à la page
                    request.setAttribute("livreRecup", livreRecup);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "deleteLivreByCote":
                // Suppression d'un livre via sa cote

                // Récupération du parametre contenant la cote du livre
                String coteLivreDelete = request.getParameter("coteLivre");

                // Suppression du livre dans la database et récupération du code de l'opération
                int codeDeleteLivre = LivreDAO.deleteLivreByCote(coteLivreDelete);

                String deleteLivreResult = "";

                // Affichage du status de l'opération
                switch (codeDeleteLivre) {
                    case 1:
                        deleteLivreResult = "Livre supprimé avec succès";
                        break;
                    case 0:
                        deleteLivreResult = "Aucun livre supprimé";
                    case -1:
                        deleteLivreResult = "Une erreur s'est produite";
                }

                // Ajout du résultat de la suppression du livre + code aux attributs renvoyés à la page
                request.setAttribute("deleteLivreResult", deleteLivreResult);
                request.setAttribute("deleteLivreCode", codeDeleteLivre);

                // Affichage console du code de l'opération
                System.out.println("Code de l'opération : " + codeDeleteLivre);
                break;

            case "getAllLivre":
                try {
                    // Récupération dans la db de la liste complète des Livre
                    Vector<Livre> livreListAll = LivreDAO.getAllLivre();

                    // Ajout de la liste des livres aux attributs renvoyés à la page
                    request.setAttribute("livreListAll", livreListAll);

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                break;
            case "updateLivre":
                // Modification de livre via sa cote

                // Récupération du parametre contenant le num de l'élève qui loue le livre
                String numUpdateLivre = request.getParameter("numLivre");
                // Récupération du parametre contenant le titre du Livre
                String titreUpdateLivre = request.getParameter("titreLivre");


                // Récupération du paramètre contenant la cote du livre
                String coteLivreUpdate = request.getParameter("coteLivre");

                try {

                    int codeUpdateNumLivre = LivreDAO.updateNumByCote(coteLivreUpdate, numUpdateLivre);
                    int codeUpdateTitre = LivreDAO.updateTitreByCote(coteLivreUpdate, titreUpdateLivre);

                    String updateLivreResult = "Une erreur s'est produite lors de la modification du livre";
                    int updateCodeLivre = codeUpdateNumLivre + codeUpdateTitre;

                    if (updateCodeLivre == 2) {
                        updateLivreResult = "Livre successfully updated";
                    }

                    System.out.println(updateLivreResult);
                    System.out.println("Code de l'update : " + updateCodeLivre);


                    // Ajout du code et du résultat de la requete aux attributs
                    request.setAttribute("updateLivreCode", updateCodeLivre);
                    request.setAttribute("updateLivreResult", updateLivreResult);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }

        this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("GET");
    }
}
