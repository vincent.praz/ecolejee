<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Get all Chambres</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getAllChambre" method="POST">
        <table>
            <tr>
                <td width="120px"><input type="submit" value="Show ALL CHAMBRE LIST"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty chambreListAll}">
        <h2>Liste des Chambres</h2>
        <table>
            <tr>
                <th>
                    No
                </th>
                <th>
                    Num
                </th>
                <th>
                    Prix
                </th>

            </tr>
            <c:forEach items="${chambreListAll}" var="chambreFromListAll">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ chambreFromListAll.no }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ chambreFromListAll.num }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ chambreFromListAll.prix }"
                                                                        default="NULL"></c:out></td>
                </tr>

            </c:forEach>


        </table>
    </c:if>


</div>
</body>
</html>