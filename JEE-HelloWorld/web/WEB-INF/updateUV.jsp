<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=updateUV"
          method="POST">

        <td style="text-align: center" width="120px">

            <c:out value="${ uvRecup.code }" default="NULL"></c:out>

            <input type="text" id="codeUV" name="codeUV" hidden
                   value="<c:out value="${ uvRecup.code }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="number" id="nbhUV" name="nbhUV"
                   value="<c:out value="${ uvRecup.nbh }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="text" id="coordUV" name="coordUV"
                   value="<c:out value="${ uvRecup.coord }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="submit" value="Update"/>
        </td>
    </form>
</tr>
