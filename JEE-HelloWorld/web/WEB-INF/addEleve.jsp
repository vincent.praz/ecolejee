<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Ajouter un nouvel eleve</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=addEleve" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="numEleve">Numero Eleve :</label></td>
                <td><input type="text" name="numEleve" id="numEleve"/></td>

                <td width="120px"><label for="nomEleve">Nom Eleve :</label></td>
                <td><input type="text" name="nomEleve" id="nomEleve"/></td>

                <td width="120px"><label for="ageEleve">Age Eleve :</label></td>
                <td><input type="text" name="ageEleve" id="ageEleve"/></td>

                <td width="120px"><label for="adresseEleve">Adresse Eleve :</label></td>
                <td><input type="text" name="adresseEleve" id="adresseEleve"/></td>

                <td width="120px"><input type="submit" value="Creer"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty addEleveResult}">
    <c:if test="${not empty addCode}">
    <c:choose>
    <c:when test="${ addCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ addEleveResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>
</body>
</html>