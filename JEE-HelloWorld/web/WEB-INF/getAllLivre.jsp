<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Get all Livre</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getAllLivre" method="POST">
        <table>
            <tr>
                <td width="120px"><input type="submit" value="Show ALL LIVRE LIST"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du livre trouvé --%>
    <c:if test="${not empty livreListAll}">
        <h2>Liste des Livres</h2>
        <table>
            <tr>
                <th>
                    Cote
                </th>
                <th>
                    Num
                </th>
                <th>
                    Titre
                </th>
                <th>
                    Date Pret
                </th>

            </tr>
            <c:forEach items="${livreListAll}" var="livreFromListAll">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ livreFromListAll.cote }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ livreFromListAll.num }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ livreFromListAll.titre }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ livreFromListAll.datepret }"
                                                                        default="NULL"></c:out></td>
                </tr>

            </c:forEach>


        </table>
    </c:if>


</div>
</body>
</html>