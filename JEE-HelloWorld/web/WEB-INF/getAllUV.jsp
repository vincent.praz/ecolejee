<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Get all UV</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getAllUV" method="POST">
        <table>
            <tr>
                <td width="120px"><input type="submit" value="Show ALL UV LIST"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'uv trouvé --%>
    <c:if test="${not empty uvListAll}">
        <h2>Liste des UVs</h2>
        <table>
            <tr>
                <th>
                    Code
                </th>
                <th>
                    Nbh
                </th>
                <th>
                    Coord
                </th>

            </tr>
            <c:forEach items="${uvListAll}" var="uvFromListAll">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ uvFromListAll.code }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ uvFromListAll.nbh }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ uvFromListAll.coord }"
                                                                        default="NULL"></c:out></td>
                </tr>

            </c:forEach>


        </table>
    </c:if>


</div>
</body>
</html>