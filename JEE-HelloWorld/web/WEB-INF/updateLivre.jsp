<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=updateLivre"
          method="POST">

        <td style="text-align: center" width="120px">

            <c:out value="${ livreRecup.cote }" default="NULL"></c:out>

            <input type="text" id="coteLivre" name="coteLivre" hidden
                   value="<c:out value="${ livreRecup.cote }" default="NULL"></c:out>"/>
        </td>


        <td style="text-align: center" width="120px">
            <input type="text" id="numLivre" name="numLivre"
                   value="<c:out value="${ livreRecup.num }" default="NULL"></c:out>"/>
        </td>


        <td style="text-align: center" width="120px">
            <input type="text" id="titreLivre" name="titreLivre"
                   value="<c:out value="${ livreRecup.titre }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <c:out value="${ livreRecup.datepret }" default="NULL"></c:out>
        </td>

        <td style="text-align: center" width="120px">
            <input type="submit" value="Update"/>
        </td>
    </form>
</tr>
