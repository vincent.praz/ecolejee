<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher/Update un Livre via sa cote</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getLivreByCote"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="coteLivre">Cote :</label></td>
                <td><input type="text" name="coteLivre" id="coteLivre"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'inscrit trouvé --%>
    <c:if test="${not empty livreRecup}">
        <h2>Livre recupere</h2>
        <table>
            <tr>
                <th>
                    Cote
                </th>
                <th>
                    Num
                </th>
                <th>
                    Titre
                </th>
                <th>
                    Date Pret
                </th>
            </tr>
            <tr>
                <td style="text-align: center" width="120px"><c:out value="${ livreRecup.cote }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ livreRecup.num }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ livreRecup.titre }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ livreRecup.datepret }"
                                                                    default="NULL"></c:out></td>
            </tr>

                <%-- Show update form --%>
            <%@include file="/WEB-INF/updateLivre.jsp" %>

        </table>
    </c:if>

    <%-- Affichage en cas d'update successful --%>
    <c:if test="${not empty updateLivreResult}">
    <c:if test="${not empty updateLivreCode}">
    <c:choose>
    <c:when test="${ updateLivreCode == 2 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ updateLivreResult }" default="NULL"></c:out>
        </div>

        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>