<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Supprimer une Chambre via son no</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=deleteChambreByNo"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="noChambre">No :</label></td>
                <td><input type="number" name="noChambre" id="noChambre"/></td>

                <td width="120px"><input type="submit" value="Supprimer"
                                         class="submit"/></td>
        </table>
    </form>


    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty deleteChambreResult}">
    <c:if test="${not empty deleteChambreCode}">
    <c:choose>
    <c:when test="${ deleteChambreCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ deleteChambreResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>