<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Ajouter un nouveau livre</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=addLivre" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="coteLivre">Cote :</label></td>
                <td><input type="text" name="coteLivre" id="coteLivre"/></td>

                <td width="120px"><label for="numLivre">Numero Eleve :</label></td>
                <td><input type="text" name="numLivre" id="numLivre"/></td>

                <td width="120px"><label for="titreLivre">Titre :</label></td>
                <td><input type="text" name="titreLivre" id="titreLivre"/></td>

                <td width="120px"><label for="datePret">Date Pret :</label></td>
                <td><input type="date" name="datePret" id="datePret"/></td>


                <td width="120px"><input type="submit" value="Creer"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty addLivreResult}">
    <c:if test="${not empty addLivreCode}">
    <c:choose>
    <c:when test="${ addLivreCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ addLivreResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>
</body>
</html>