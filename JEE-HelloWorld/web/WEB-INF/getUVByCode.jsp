<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher/Update un UV via son code</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getUVByCode"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="codeUV">Code :</label></td>
                <td><input type="text" name="codeUV" id="codeUV"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'uv trouvé --%>
    <c:if test="${not empty uvRecup}">
        <h2>UV recupere</h2>
        <table>
            <tr>
                <th>
                    Code
                </th>
                <th>
                    Nbh
                </th>
                <th>
                    Coord
                </th>
            </tr>
            <tr>
                <td style="text-align: center" width="120px"><c:out value="${ uvRecup.code }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ uvRecup.nbh }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ uvRecup.coord }"
                                                                    default="NULL"></c:out></td>
            </tr>

                <%-- Show update form --%>
            <%@include file="/WEB-INF/updateUV.jsp" %>

        </table>
    </c:if>

    <%-- Affichage en cas d'update successful --%>
    <c:if test="${not empty updateUVResult}">
    <c:if test="${not empty updateUVCode}">
    <c:choose>
    <c:when test="${ updateUVCode == 2 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ updateUVResult }" default="NULL"></c:out>
        </div>

        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>