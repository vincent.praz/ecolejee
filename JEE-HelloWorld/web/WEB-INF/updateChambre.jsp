<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=updateChambre"
          method="POST">

        <td style="text-align: center" width="120px">

            <c:out value="${ chambreRecup.no }" default="NULL"></c:out>

            <input type="text" id="noChambre" name="noChambre" hidden
                   value="<c:out value="${ chambreRecup.no }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="text" id="numChambre" name="numChambre"
                   value="<c:out value="${ chambreRecup.num }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="number" step="0.1" id="prixChambre" name="prixChambre"
                   value="<c:out value="${ chambreRecup.prix }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="submit" value="Update"/>
        </td>
    </form>
</tr>
