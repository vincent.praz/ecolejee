<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher des Eleves via leur nom</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getEleveByNom" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="nomEleve">Nom Eleve :</label></td>
                <td><input type="text" name="nomEleve" id="nomEleve"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty eleveListRecupByNom}">
        <h2>Liste des eleves ayant le meme nom</h2>
        <table>
            <tr>
                <th>
                    Num
                </th>
                <th>
                    No
                </th>
                <th>
                    Adresse
                </th>
                <th>
                    Age
                </th>

            </tr>
            <c:forEach items="${eleveListRecupByNom}" var="eleveFromListByNom">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByNom.num }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByNom.no }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByNom.adresse }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByNom.age }" default="NULL"></c:out></td>
                </tr>

            </c:forEach>



        </table>
    </c:if>


</div>
</body>
</html>