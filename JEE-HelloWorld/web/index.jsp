<%--
  Created by IntelliJ IDEA.
  User: Zelyvanna
  Date: 02.04.2020
  Time: 09:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <meta charset="UTF-8">
    <title>JEE - Rendu</title>
</head>
<body>

<hr>
<hr>


<center><h1>ELEVE</h1></center>
<%-- Formulaire de recherche + update d'élève by num --%>
<%@include file="/WEB-INF/getEleveByNum.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de recherche d'élèves by Nom --%>
<%@include file="/WEB-INF/getEleveByNom.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de recherche d'élèves by age --%>
<%@include file="/WEB-INF/getEleveByAnnee.jsp" %>


<br><br>
<hr>
<br><br>

<%-- Formulaire d'ajout d'élève --%>
<%@include file="/WEB-INF/addEleve.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de suppression d'élève --%>
<%@include file="/WEB-INF/deleteEleveByNum.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire d'affichage de tous les élèves --%>
<%@include file="/WEB-INF/getAllEleve.jsp" %>

<br><br>
<hr>
<hr>
<br><br>


<center><h1>INSCRIT</h1></center>

<%-- Formulaire d'ajout d'inscrit --%>
<%@include file="/WEB-INF/addInscrit.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de suppression d'inscrit --%>
<%@include file="/WEB-INF/deleteInscritByCode.jsp" %>

<br><br>
<hr>
<br><br>


<%-- Formulaire de récupération de tous les inscrits --%>
<%@include file="/WEB-INF/getAllInscrit.jsp" %>


<br><br>
<hr>
<br><br>


<%-- Formulaire de récupération d'un inscrit via son code --%>
<%@include file="/WEB-INF/getInscritByCode.jsp" %>

<br><br>
<hr>
<hr>
<br><br>

<center><h1>CHAMBRE</h1></center>

<%-- Formulaire de récupération d'une chambre via son no --%>
<%@include file="/WEB-INF/getChambreByNo.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de suppression d'une chambre via son no --%>
<%@include file="/WEB-INF/deleteChambreByNo.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire d'ajout d'une chambre --%>
<%@include file="/WEB-INF/addChambre.jsp" %>

<br><br>
<hr>
<br><br>


<%-- Formulaire d'affichage des chambres --%>
<%@include file="/WEB-INF/getAllChambre.jsp" %>

<br><br>
<hr>
<hr>
<br><br>

<center><h1>UV</h1></center>

<%-- Formulaire d'ajout des UV --%>
<%@include file="/WEB-INF/addUV.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire d'affichage des UV --%>
<%@include file="/WEB-INF/getAllUV.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire d'update/get des UV --%>
<%@include file="/WEB-INF/getUVByCode.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de delete des UV --%>
<%@include file="/WEB-INF/deleteUVByCode.jsp" %>

<br><br>
<hr>
<hr>
<br><br>

<center><h1>LIVRE</h1></center>


<%-- Formulaire d'ajout des Livres --%>
<%@include file="/WEB-INF/addLivre.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de recherche et update des Livres --%>
<%@include file="/WEB-INF/getLivreByCote.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire de recherche et update des Livres --%>
<%@include file="/WEB-INF/deleteLivreByCote.jsp" %>

<br><br>
<hr>
<br><br>

<%-- Formulaire d'affichage des Livres --%>
<%@include file="/WEB-INF/getAllLivre.jsp" %>

<br><br>
<hr>
<br><br>

</body>
</html>