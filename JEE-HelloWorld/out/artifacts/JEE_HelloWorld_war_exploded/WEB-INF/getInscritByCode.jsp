<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher/Update un Inscrit via son Code</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getInscritByCode"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="codeInscrit">Code :</label></td>
                <td><input type="text" name="codeInscrit" id="codeInscrit"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'inscrit trouvé --%>
    <c:if test="${not empty inscritRecup}">
        <h2>Inscrit recupere</h2>
        <table>
            <tr>
                <th>
                    Num
                </th>
                <th>
                    Code
                </th>
                <th>
                    Note
                </th>
            </tr>
            <tr>
                <td style="text-align: center" width="120px"><c:out value="${ inscritRecup.num }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ inscritRecup.code }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ inscritRecup.note }"
                                                                    default="NULL"></c:out></td>
            </tr>

                <%-- Show update form --%>
            <%@include file="/WEB-INF/updateInscritByCodeOrNum.jsp" %>

        </table>
    </c:if>

    <%-- Affichage en cas d'update successful --%>
    <c:if test="${not empty updateInscritResult}">
    <c:if test="${not empty updateInscritCode}">
    <c:choose>
    <c:when test="${ updateInscritCode == 2 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ updateInscritResult }" default="NULL"></c:out>
        </div>

        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>