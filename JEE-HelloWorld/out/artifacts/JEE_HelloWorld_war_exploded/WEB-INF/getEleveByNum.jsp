<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher/Update un Eleve via son num</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getEleveByNum" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="numelev">Numero Eleve :</label></td>
                <td><input type="text" name="numelev" id="numelev"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty eleveRecup}">
        <h2>Eleve recupere</h2>
        <table>
            <tr>
                <th>
                    Num
                </th>
                <th>
                    No
                </th>
                <th>
                    Adresse
                </th>
                <th>
                    Age
                </th>

            </tr>
            <tr>
                <td style="text-align: center" width="120px"><c:out value="${ eleveRecup.num }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ eleveRecup.no }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ eleveRecup.adresse }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ eleveRecup.age }"
                                                                    default="NULL"></c:out></td>
            </tr>

                <%-- Show update form --%>
            <%@include file="/WEB-INF/updateEleveByNum.jsp" %>

        </table>
    </c:if>

    <%-- Affichage en cas d'update successful --%>
    <c:if test="${not empty updateEleveResult}">
    <c:if test="${not empty updateCode}">
    <c:choose>
    <c:when test="${ updateCode == 2 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ updateEleveResult }" default="NULL"></c:out>
        </div>

        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>