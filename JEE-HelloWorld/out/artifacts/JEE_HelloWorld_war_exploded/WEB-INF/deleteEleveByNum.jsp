<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Supprimer un Eleve via son num</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=deleteEleveByNum"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="numEleve">Numero Eleve :</label></td>
                <td><input type="text" name="numEleve" id="numEleve"/></td>

                <td width="120px"><input type="submit" value="Supprimer"
                                         class="submit"/></td>
        </table>
    </form>


    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty deleteEleveResult}">
    <c:if test="${not empty deleteCode}">
    <c:choose>
    <c:when test="${ deleteCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ deleteEleveResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>