<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Get all eleves</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getAllEleve" method="POST">
        <table>
            <tr>
                <td width="120px"><input type="submit" value="Show ALL ELEVE LIST"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty eleveListAll}">
        <h2>Liste des eleves</h2>
        <table>
            <tr>
                <th>
                    Num
                </th>
                <th>
                    No
                </th>
                <th>
                    Adresse
                </th>
                <th>
                    Age
                </th>

            </tr>
            <c:forEach items="${eleveListAll}" var="eleveFromListAll">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListAll.num }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListAll.no }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListAll.adresse }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListAll.age }" default="NULL"></c:out></td>
                </tr>

            </c:forEach>



        </table>
    </c:if>


</div>
</body>
</html>