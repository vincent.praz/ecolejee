<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Ajouter un nouvel Inscrit</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=addInscrit" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="numEleve">Numero Eleve :</label></td>
                <td><input type="text" name="numEleve" id="numEleve"/></td>

                <td width="120px"><label for="codeInscrit">Code inscrit :</label></td>
                <td><input type="text" name="codeInscrit" id="codeInscrit"/></td>

                <td width="120px"><label for="noteInscrit">Note :</label></td>
                <td><input type="text" name="noteInscrit" id="noteInscrit"/></td>

                <td width="120px"><input type="submit" value="Creer"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty addInscritResult}">
    <c:if test="${not empty addInscritCode}">
    <c:choose>
    <c:when test="${ addInscritCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ addInscritResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>
</body>
</html>