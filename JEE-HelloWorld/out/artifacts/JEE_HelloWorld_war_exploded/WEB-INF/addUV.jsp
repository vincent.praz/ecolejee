<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Ajouter un nouvel UV</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=addUV" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="codeUV">Code :</label></td>
                <td><input type="text" name="codeUV" id="codeUV"/></td>

                <td width="120px"><label for="nbhUV">Nbh :</label></td>
                <td><input type="number" name="nbhUV" id="nbhUV"/></td>

                <td width="120px"><label for="coordUV">Coord :</label></td>
                <td><input type="text" name="coordUV" id="coordUV"/></td>

                <td width="120px"><input type="submit" value="Creer" class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty addUVResult}">
    <c:if test="${not empty addUVCode}">
    <c:choose>
    <c:when test="${ addUVCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ addUVResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>
</body>
</html>