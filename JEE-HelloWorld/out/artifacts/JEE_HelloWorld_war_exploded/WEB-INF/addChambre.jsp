<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Ajouter une nouvelle Chambre</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=addChambre" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="noChambre">No :</label></td>
                <td><input type="number" name="noChambre" id="noChambre"/></td>

                <td width="120px"><label for="numChambre">Num :</label></td>
                <td><input type="text" name="numChambre" id="numChambre"/></td>

                <td width="120px"><label for="prixChambre">Prix :</label></td>
                <td><input type="text" name="prixChambre" id="prixChambre"/></td>

                <td width="120px"><input type="submit" value="Creer"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage du résultat de l'ajout (Changement de la couleur de la div si erreur ou success --%>
    <c:if test="${not empty addChambreResult}">
    <c:if test="${not empty addChambreCode}">
    <c:choose>
    <c:when test="${ addChambreCode == 1 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ addChambreResult }" default="NULL"></c:out>
        </div>
        </c:if>
        </c:if>
    </div>
</body>
</html>