<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=updateInscritByCodeOrNum"
          method="POST">


        <td style="text-align: center" width="120px">
            <input type="text" id="numInscrit" name="numInscrit"
                   value="<c:out value="${ inscritRecup.num }" default="NULL"></c:out>"/>
        </td>
        <td style="text-align: center" width="120px">

            <c:out value="${ inscritRecup.code }" default="NULL"></c:out>

            <input type="text" id="codeInscrit" name="codeInscrit" hidden
                   value="<c:out value="${ inscritRecup.code }" default="NULL"></c:out>"/>
        </td>
        <td style="text-align: center" width="120px">
            <input type="number" step="0.1" id="noteInscrit" name="noteInscrit"
                   value="<c:out value="${ inscritRecup.note }" default="NULL"></c:out>"/>
        </td>

        <td style="text-align: center" width="120px">
            <input type="submit" value="Update"/>
        </td>
    </form>
</tr>
