<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher/Update une Chambre via son No</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getChambreByNo"
          method="POST">
        <table>
            <tr>
                <td width="120px"><label for="noChambre">No :</label></td>
                <td><input type="number" name="noChambre" id="noChambre"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de la chambre trouvée --%>
    <c:if test="${not empty chambreRecup}">
        <h2>Chambre recuperee</h2>
        <table>
            <tr>
                <th>
                    No
                </th>
                <th>
                    Num
                </th>
                <th>
                    Prix
                </th>
            </tr>
            <tr>
                <td style="text-align: center" width="120px"><c:out value="${ chambreRecup.no }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ chambreRecup.num }"
                                                                    default="NULL"></c:out></td>
                <td style="text-align: center" width="120px"><c:out value="${ chambreRecup.prix }"
                                                                    default="NULL"></c:out></td>
            </tr>

                <%-- Show update form --%>
                <%@include file="/WEB-INF/updateChambre.jsp" %>

        </table>
    </c:if>

    <%-- Affichage en cas d'update successful --%>
    <c:if test="${not empty updateChambreResult}">
    <c:if test="${not empty updateChambreCode}">
    <c:choose>
    <c:when test="${ updateChambreCode == 2 }">
    <div style="background-color: greenyellow">
        </c:when>
        <c:otherwise>
        <div style="background-color: red">
            </c:otherwise>

            </c:choose>
            <c:out value="${ updateChambreResult }" default="NULL"></c:out>
        </div>

        </c:if>
        </c:if>
    </div>


</div>
</body>
</html>