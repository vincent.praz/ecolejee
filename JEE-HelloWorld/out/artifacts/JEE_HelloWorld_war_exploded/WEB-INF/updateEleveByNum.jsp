<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=updateEleveByNum"
          method="POST">
        <input type="text" id="numEleve" name="numEleve" hidden
               value="<c:out value="${ eleveRecup.num }" default="NULL"></c:out>"/>

        <td style="text-align: center" width="120px">
            <c:out value="${ eleveRecup.num }" default="NULL"></c:out>
        </td>
        <td style="text-align: center" width="120px">
            <input type="number" id="noEleve" name="noEleve"
                   value=" <c:out value="${ eleveRecup.no }" default="NULL"></c:out>"/>
        </td>
        <td style="text-align: center" width="120px">
            <input type="text" id="adresseEleve" name="adresseEleve"
                   value="<c:out value="${ eleveRecup.adresse }" default="NULL"></c:out>"/>
        </td>
        <td style="text-align: center" width="120px">
            <c:out value="${ eleveRecup.age }" default="NULL"></c:out>
        </td>
        <td style="text-align: center" width="120px">
            <input type="submit" value="Update"/>
        </td>
    </form>
</tr>
