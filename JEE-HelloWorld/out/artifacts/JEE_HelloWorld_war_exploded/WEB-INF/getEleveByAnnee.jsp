<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Chercher des Eleves via leur année de naissance</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getListEleveByDateN" method="POST">
        <table>
            <tr>
                <td width="120px"><label for="anneeNaissance">Annee Naissance Eleve :</label></td>
                <td><input type="number" name="anneeNaissance" id="anneeNaissance"/></td>

                <td width="120px"><input type="submit" value="Valider"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty eleveListRecupByAge}">
        <h2>Liste des eleves ayant le meme age</h2>
        <table>
            <tr>
                <th>
                    Num
                </th>
                <th>
                    No
                </th>
                <th>
                    Adresse
                </th>
                <th>
                    Age
                </th>

            </tr>
            <c:forEach items="${eleveListRecupByAge}" var="eleveFromListByAge">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByAge.num }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByAge.no }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByAge.adresse }" default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ eleveFromListByAge.age }" default="NULL"></c:out></td>
                </tr>

            </c:forEach>



        </table>
    </c:if>


</div>
</body>
</html>