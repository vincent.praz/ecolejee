<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Formulaire</title>
</head>
<body>
<div align="center">
    <h1 class="pgtitre">Get all inscrits</h1>
    <form name="id" class="form" action="/JEE_HelloWorld_war/ControleurPrincipal?idaction=getAllInscrit" method="POST">
        <table>
            <tr>
                <td width="120px"><input type="submit" value="Show ALL INSCRIT LIST"
                                         class="submit"/></td>
        </table>
    </form>

    <%-- Affichage de l'élève trouvé --%>
    <c:if test="${not empty inscritListAll}">
        <h2>Liste des inscrits</h2>
        <table>
            <tr>
                <th>
                    Code
                </th>
                <th>
                    Num
                </th>
                <th>
                    Note
                </th>

            </tr>
            <c:forEach items="${inscritListAll}" var="inscritFromListAll">
                <tr>
                    <td style="text-align: center" width="120px"><c:out value="${ inscritFromListAll.code }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ inscritFromListAll.num }"
                                                                        default="NULL"></c:out></td>
                    <td style="text-align: center" width="120px"><c:out value="${ inscritFromListAll.note }"
                                                                        default="NULL"></c:out></td>
                </tr>

            </c:forEach>


        </table>
    </c:if>


</div>
</body>
</html>