package com.crea.dev2.a2.jee.ecole.model.test;

import com.crea.dev2.a2.jee.ecole.model.beans.Chambre;
import com.crea.dev2.a2.jee.ecole.model.beans.Eleve;
import com.crea.dev2.a2.jee.ecole.model.dao.ChambreDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import static com.crea.dev2.a2.jee.ecole.model.dao.ChambreDAO.*;
import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.*;
import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.deleteEleveByNum;
import static org.junit.Assert.*;

public class ChambreDAOTest {

    @Before
    public void testAddChambre() throws Exception {
        // Création des élèves dans la database avec un no null
        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
        Eleve e2 = new Eleve("KAMTO005", 2, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
        Eleve e3 = new Eleve("LAURENCY004", 3, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
        Eleve e4 = new Eleve("TABIS003", 4, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
        Eleve e5 = new Eleve("TAHAE002", 5, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

        listEleve.add(e1);
        listEleve.add(e2);
        listEleve.add(e3);
        listEleve.add(e4);
        listEleve.add(e5);


        for (Eleve eleve : listEleve) {
            assertEquals(1, addEleve(eleve.getNum(), eleve.getNom(), eleve.getAge(), eleve.getAdresse()));
        }

        // Création dans la database des chambres avec un num null
        Chambre ch1 = new Chambre(1, "AGUE001", 350);
        Chambre ch2 = new Chambre(2, "KAMTO005", 400);
        Chambre ch3 = new Chambre(3, "LAURENCY004", 50);
        Chambre ch4 = new Chambre(4, "TABIS003", 400);
        Chambre ch5 = new Chambre(5, "TAHAE002", 250);

        ArrayList<Chambre> chambreList = new ArrayList<>();
        chambreList.add(ch1);
        chambreList.add(ch2);
        chambreList.add(ch3);
        chambreList.add(ch4);
        chambreList.add(ch5);

        for (Chambre chambre : chambreList) {
            assertEquals(1, addChambre(chambre.getNo(), chambre.getPrix()));
        }

        // Pour chaque élève, on lui attribue un no de chambre et on attribue à sa chambre le num de l'élève en db
        int i = 1;
        for (Eleve eleve : listEleve) {

            System.out.println("Updating eleve num" + eleve.getNum() + " setting no chambre " + i);
            updNoChambreEleveByNum(eleve.getNum(), i);
            System.out.println("Updating ch " + i + " setting num " + eleve.getNum());
            ChambreDAO.updateNumByNo(eleve.getNum(), i);
            i++;
        }
    }

    @After
    public void testDeleteChambreByNo() throws Exception {
        // On set les no de chambre à null dans la table élève
        updNoChambreEleveByNum("AGUE001");
        updNoChambreEleveByNum("KAMTO005");
        updNoChambreEleveByNum("LAURENCY004");
        updNoChambreEleveByNum("TABIS003");
        updNoChambreEleveByNum("TAHAE002");


        // On set les num d'élève a null dans la table chambre
        updateNumByNo(1);
        updateNumByNo(2);
        updateNumByNo(3);
        updateNumByNo(4);
        updateNumByNo(5);


        // Suppression des élèves
        assertEquals(1, deleteEleveByNum("AGUE001"));
        assertEquals(1, deleteEleveByNum("KAMTO005"));
        assertEquals(1, deleteEleveByNum("LAURENCY004"));
        assertEquals(1, deleteEleveByNum("TABIS003"));
        assertEquals(1, deleteEleveByNum("TAHAE002"));

        // Suppression des chambres
        assertEquals(1, deleteChambreByNo(1));
        assertEquals(1, deleteChambreByNo(2));
        assertEquals(1, deleteChambreByNo(3));
        assertEquals(1, deleteChambreByNo(4));
        assertEquals(1, deleteChambreByNo(5));
    }

    @Test
    public void testGetChambreByNo() throws SQLException {
        Chambre ch1 = new Chambre(1, "AGUE001", 350);
        assertEquals(ch1.getNo(), getChambreByNo(1).getNo());
        assertEquals(ch1.getNum(), getChambreByNo(1).getNum());
        assertEquals(ch1.getPrix(), getChambreByNo(1).getPrix(), 0);
    }

    @Test
    public void testGetAllChambre() throws SQLException {
        Vector<Chambre> chambreList = getAllChambre();
        assertNotNull(chambreList);
        assertNotEquals(0, chambreList.size());
    }

    @Test
    public void testUpdatePrixByNum() throws SQLException {
        // Changement de prix
        assertEquals(1, updatePrixByNum("AGUE001", 360));

        //Récupération de la chambre updatée
        Chambre ch1 = getChambreByNo(1);

        // Vérification du bon changement du prix
        assertEquals(360, ch1.getPrix(), 0);
    }
}