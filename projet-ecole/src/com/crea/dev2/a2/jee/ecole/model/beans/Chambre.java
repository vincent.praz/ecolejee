package com.crea.dev2.a2.jee.ecole.model.beans;

public class Chambre {
    private int no;
    private String num;
    private float prix;

    /**
     * Constructeur de l'objet chambre
     *
     * @param no numéro identifiant de la chambre
     * @param num numéro de l'élève
     * @param prix prix de la chambre
     */
    public Chambre(int no, String num, float prix) {
        this.no = no;
        this.num = num;
        this.prix = prix;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
