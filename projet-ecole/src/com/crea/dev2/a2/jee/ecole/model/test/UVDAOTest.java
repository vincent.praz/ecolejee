package com.crea.dev2.a2.jee.ecole.model.test;

import com.crea.dev2.a2.jee.ecole.model.beans.UV;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import static com.crea.dev2.a2.jee.ecole.model.dao.UVDAO.*;
import static org.junit.Assert.*;

public class UVDAOTest {

    @Before
    public void testAddUV() {
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        UV uv2 = new UV("maths_info_Grp1", 26, "Mme ASSELAH");
        UV uv3 = new UV("Web_Service_Grp1", 10, "Mr PLASSE");

        ArrayList<UV> uvList = new ArrayList<>();

        uvList.add(uv1);
        uvList.add(uv2);
        uvList.add(uv3);

        for (UV uv : uvList) {
            assertEquals(1, addUV(uv.getCode(), uv.getNbh(), uv.getCoord()));
        }

    }

    @After
    public void testDeleteUVByCode() {
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        UV uv2 = new UV("maths_info_Grp1", 26, "Mme ASSELAH");
        UV uv3 = new UV("Web_Service_Grp1", 10, "Mr PLASSE");

        ArrayList<UV> uvList = new ArrayList<>();

        uvList.add(uv1);
        uvList.add(uv2);
        uvList.add(uv3);

        for (UV uv : uvList) {
            assertEquals(1, deleteUVByCode(uv.getCode()));
        }
    }

    @Test
    public void testGetUVByCode() throws SQLException {
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        UV uvTest = getUVByCode(uv1.getCode());

        assertEquals(uv1.getCoord(), uvTest.getCoord());
        assertEquals(uv1.getNbh(), uvTest.getNbh());
        assertEquals(uv1.getCode(), uvTest.getCode());
    }


    @Test
    public void testGetAllUV() throws SQLException {
        Vector<UV> uv = getAllUV();
        assertNotNull(uv);
        assertNotEquals(0, uv.size());
    }

    @Test
    public void testUpdateCoordByCode() throws SQLException {
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        assertEquals(1, updateCoordByCode(uv1.getCode(), "Mr JOESTAR"));
        UV uvTest = getUVByCode(uv1.getCode());
        assertNotEquals(uv1.getCoord(), uvTest.getCoord());
        assertEquals("Mr JOESTAR", uvTest.getCoord());
    }

    @Test
    public void testUpdateNbhByCode() throws SQLException {
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        assertEquals(1, updateNbhByCode(uv1.getCode(), 40));
        UV uvTest = getUVByCode(uv1.getCode());
        assertNotEquals(uv1.getNbh(), uvTest.getNbh());
        assertEquals(40, uvTest.getNbh());
    }
}