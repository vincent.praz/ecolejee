package com.crea.dev2.a2.jee.ecole.model.dao;

import com.crea.dev2.a2.jee.ecole.model.beans.UV;
import com.crea.dev2.a2.jee.ecole.model.utils.DBAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class UVDAO {

    /**
     * Liste un UV en fonctions de son code
     *
     * @param code code de l'uv
     * @return Retourne l'objet UV récupéré
     * @throws SQLException
     */
    public static UV getUVByCode(String code) throws SQLException {
        DBAction.DBConnexion(); // test conn
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM uv WHERE code = '" + code + "';");
        resultSet.first(); // Starting the resultset at the first position
        UV UVTemp = new UV(resultSet.getString("code"), resultSet.getInt("nbh"), resultSet.getString("coord"));
        resultSet.close(); // Fermeture du resultSet
        DBAction.DBClose();
        return UVTemp;
    }


    /**
     * Delete UV par code
     *
     * @param code code de l'uv à supprimer
     * @return 1 ou 0 (le nbr d'uv supprimés) sinon le (-) code d'erreur
     */
    public static int deleteUVByCode(String code) {
        int result; // résultat de la requête
        DBAction.DBConnexion(); // test conn
        try {
            result = DBAction.getStm().executeUpdate("DELETE FROM uv WHERE code = '" + code + "';");
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }

    /**
     * Ajouter un UV
     *
     * @param code  code de l'uv
     * @param nbh   nombre d'heure de cours
     * @param coord coordonnées
     * @return 1 ou 0 (le nbr d'uv ajouté) sinon le (-) code d'erreur
     */
    public static int addUV(String code, int nbh, String coord) {
        DBAction.DBConnexion(); // test conn
        int result; //résultat de la requête
        // Attention a bien mettre des '' pour les strings
        String SQL = "INSERT INTO uv(code,nbh,coord) VALUES('" + code + "'," + nbh + ",'" + coord + "');";
        try {
            result = DBAction.getStm().executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            result = e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }

    /**
     * La liste des UV
     *
     * @return : la liste de tous les uv.
     * @throws SQLException
     */
    public static Vector<UV> getAllUV() throws SQLException {
        DBAction.DBConnexion(); // test conn
        Vector<UV> vUV = new Vector<>();
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM uv;");
        while (resultSet.next()) {
            UV uvTemp = new UV(resultSet.getString("code"), resultSet.getInt("nbh"), resultSet.getString("coord"));
            vUV.add(uvTemp);
        }
        DBAction.DBClose();
        return vUV;
    }

    /**
     * Met a jour la coordonnée d'un uv par son code
     *
     * @param code  code de l'uv à modifier
     * @param coord nouvelle coordonnée
     * @return 1 ou 0  (le nbr d'uv mis à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updateCoordByCode(String code, String coord) throws SQLException {
        int result;
        DBAction.DBConnexion();
        String req = "UPDATE uv SET coord = '" + coord + "' WHERE code ='" + code + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }

        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Met à jour le nombre d'heure de l'uv par son code
     *
     * @param code code de l'uv à modifier
     * @param nbh  nouveau nombre d'heures
     * @return 1 ou 0  (le nbr d'uv mis à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updateNbhByCode(String code, int nbh) throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE uv SET nbh = " + nbh + " WHERE code ='" + code + "' ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }
}
