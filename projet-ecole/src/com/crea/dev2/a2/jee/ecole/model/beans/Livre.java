package com.crea.dev2.a2.jee.ecole.model.beans;

import java.util.Date;

public class Livre {
    private String cote;
    private String num;
    private String titre; // NOT NULL ON DB
    private Date datepret;

    /**
     * Constructeur du l'objet Livre
     *
     * @param cote cote du livre
     * @param num numéro identifiant de l'élève
     * @param titre titre du livre
     * @param datepret Date de prêt du livre
     */
    public Livre(String cote, String num, String titre, Date datepret) {
        this.cote = cote;
        this.num = num;
        this.titre = titre;
        this.datepret = datepret;
    }

    public String getCote() {
        return cote;
    }

    public void setCote(String cote) {
        this.cote = cote;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Date getDatepret() {
        return datepret;
    }

    public void setDatepret(Date datepret) {
        this.datepret = datepret;
    }
}
