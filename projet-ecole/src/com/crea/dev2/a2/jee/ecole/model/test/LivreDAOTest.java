package com.crea.dev2.a2.jee.ecole.model.test;

import com.crea.dev2.a2.jee.ecole.model.beans.Eleve;
import com.crea.dev2.a2.jee.ecole.model.beans.Livre;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.addEleve;
import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.deleteEleveByNum;
import static com.crea.dev2.a2.jee.ecole.model.dao.LivreDAO.*;
import static org.junit.Assert.*;

public class LivreDAOTest {

    @Before
    public void testAddLivre() throws ParseException, SQLException {
        // Création élèves
        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
        Eleve e2 = new Eleve("KAMTO005", 2, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
        Eleve e3 = new Eleve("LAURENCY004", 3, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
        Eleve e4 = new Eleve("TABIS003", 4, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");

        listEleve.add(e1);
        listEleve.add(e2);
        listEleve.add(e3);
        listEleve.add(e4);

        for (Eleve eleve : listEleve) {
            assertEquals(1, addEleve(eleve.getNum(), eleve.getNom(), eleve.getAge(), eleve.getAdresse()));
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Date date = simpleDateFormat.parse("2018-09-12");
        Date date1 = simpleDateFormat.parse("2019-12-09");
        Date date2 = simpleDateFormat.parse("2020-08-09");
        Date date3 = simpleDateFormat.parse("1998-04-08");

        ArrayList<Livre> listLivre = new ArrayList<>();

        Livre l1 = new Livre("ISBN10000", "AGUE001", "Un vase dhonneur", date);
        Livre l2 = new Livre("ISBN10001", "KAMTO005", "Seul au monde", date1);
        Livre l3 = new Livre("ISBN10002", "LAURENCY004", "Meutre à la maison blanche", date2);
        Livre l4 = new Livre("ISBN10003", "TABIS003", "Double Impact", date3);

        listLivre.add(l1);
        listLivre.add(l2);
        listLivre.add(l3);
        listLivre.add(l4);

        for (Livre livre : listLivre) {
            assertEquals(1, addLivre(livre.getCote(), livre.getNum(), livre.getTitre(), livre.getDatepret()));
        }

    }

    @After
    public void testDeleteLivreByCote() {
        assertEquals(1, deleteLivreByCote("ISBN10000"));
        assertEquals(1, deleteLivreByCote("ISBN10001"));
        assertEquals(1, deleteLivreByCote("ISBN10002"));
        assertEquals(1, deleteLivreByCote("ISBN10003"));

        assertEquals(1, deleteEleveByNum("AGUE001"));
        assertEquals(1, deleteEleveByNum("KAMTO005"));
        assertEquals(1, deleteEleveByNum("LAURENCY004"));
        assertEquals(1, deleteEleveByNum("TABIS003"));
    }

    @Test
    public void testGetLivreByCote() throws ParseException, SQLException {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = simpleDateFormat.parse("2018-09-12");
        Livre lRef = new Livre("ISBN10000", "AGUE001", "Un vase dhonneur", date);
        Livre lTest = getLivreByCote("ISBN10000");

        assertEquals(lRef.getCote(), lTest.getCote());
        assertEquals(lRef.getNum(), lTest.getNum());
        assertEquals(lRef.getTitre(), lTest.getTitre());
    }

    @Test
    public void testGetAllLivre() throws SQLException {
        Vector<Livre> vLivres = getAllLivre();

        assertNotNull(vLivres);
        assertNotEquals(0, vLivres.size());
    }

    @Test
    public void testUpdateNumByCote() throws SQLException {
        assertEquals(1, updateNumByCote("ISBN10000", "TABIS003"));

        // Récupération du livre updaté
        Livre lUpdated = getLivreByCote("ISBN10000");
        assertEquals(lUpdated.getNum(), "TABIS003");
    }

    @Test
    public void testUpdateTitreByCote() throws SQLException {
        assertEquals(1, updateTitreByCote("ISBN10000", "testTitre"));

        // Récupération du livre updaté
        Livre lUpdated = getLivreByCote("ISBN10000");
        assertEquals(lUpdated.getTitre(), "testTitre");
    }
}