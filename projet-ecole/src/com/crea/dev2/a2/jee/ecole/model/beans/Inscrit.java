package com.crea.dev2.a2.jee.ecole.model.beans;


public class Inscrit {
    private String code;
    private String num;
    private float note;

    /**
     * Constructeur de l'objet Inscrit
     *
     * @param code Id Code de l'uv
     * @param num numéro d'identifiant de l'élève
     * @param note Note accordée à un élève sur une matière ou UV
     */
    public Inscrit(String code, String num, float note) {
        this.code = code;
        this.num = num;
        this.note = note;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }
}
