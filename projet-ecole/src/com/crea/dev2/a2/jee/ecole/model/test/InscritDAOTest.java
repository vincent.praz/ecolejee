package com.crea.dev2.a2.jee.ecole.model.test;

import com.crea.dev2.a2.jee.ecole.model.beans.Eleve;
import com.crea.dev2.a2.jee.ecole.model.beans.Inscrit;
import com.crea.dev2.a2.jee.ecole.model.beans.UV;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Vector;

import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.addEleve;
import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.deleteEleveByNum;
import static com.crea.dev2.a2.jee.ecole.model.dao.InscritDAO.*;
import static com.crea.dev2.a2.jee.ecole.model.dao.UVDAO.addUV;
import static com.crea.dev2.a2.jee.ecole.model.dao.UVDAO.deleteUVByCode;
import static org.junit.Assert.*;

public class InscritDAOTest {
    @Before
    public void testAddInscrit() {
        // Création uv
        UV uv1 = new UV("JAVA_GRP1", 30, "Mr RIDENE");
        assertEquals(1, addUV(uv1.getCode(), uv1.getNbh(), uv1.getCoord()));

        // Creation élève
        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
        assertEquals(1, addEleve(e1.getNum(), e1.getNom(), e1.getAge(), e1.getAdresse()));
        Eleve e2 = new Eleve("KAMTO005", 2, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
        assertEquals(1, addEleve(e2.getNum(), e2.getNom(), e2.getAge(), e2.getAdresse()));

        // Création inscrit
        Inscrit i1 = new Inscrit(uv1.getCode(), e1.getNum(), 2);
        assertEquals(1, addInscrit(i1.getCode(), i1.getNum(), i1.getNote()));
    }

    @After
    public void testDeleteInscritByCode() {
        assertEquals(1, deleteInscritByCode("JAVA_GRP1"));
        assertEquals(1, deleteUVByCode("JAVA_GRP1"));
        assertEquals(1, deleteEleveByNum("AGUE001"));
        assertEquals(1, deleteEleveByNum("KAMTO005"));
    }

    @Test
    public void testGetInscritByCode() throws SQLException {
        Inscrit i1 = new Inscrit("JAVA_GRP1", "AGUE001", 2);
        Inscrit iTest = getInscritByCode("JAVA_GRP1");

        assertEquals(i1.getCode(), iTest.getCode());
        assertEquals(i1.getNum(), iTest.getNum());
        assertEquals(i1.getNote(), iTest.getNote(), 0);
    }

    @Test
    public void testGetAllInscrit() throws SQLException {
        Vector<Inscrit> vInscrits = getAllInscrit();
        assertNotNull(vInscrits);
        assertNotEquals(0, vInscrits.size());
    }


    @Test
    public void testUpdateNoteByNum() throws SQLException {
        Inscrit i1 = new Inscrit("JAVA_GRP1", "AGUE001", 2);
        assertEquals(1, updateNotebyNum(i1.getNum(), 6));
        Inscrit iTest = getInscritByCode(i1.getCode());
        assertNotEquals(i1.getNote(), iTest.getNote());
        assertEquals(6, iTest.getNote(), 0);
    }


    @Test
    public void testUpdateNumByCode() throws SQLException {
        Inscrit i1 = new Inscrit("JAVA_GRP1", "AGUE001", 2);
        assertEquals(1, updateNumByCode(i1.getCode(), "KAMTO005"));
        Inscrit iTest = getInscritByCode(i1.getCode());
        assertNotEquals(i1.getNum(), iTest.getNum());
        assertEquals("KAMTO005", iTest.getNum());
    }
}